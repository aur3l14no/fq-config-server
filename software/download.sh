#!/bin/bash

# release_url="https://github.com/XTLS/Xray-core/releases/download"
# release_ver="v1.5.5"
# declare -a release_files=(
#     "Xray-linux-64.zip"
#     "Xray-macos-64.zip"
#     "Xray-windows-64.zip"
#     "Xray-macos-arm64-v8a.zip"
#     # "Xray-linux-arm64-v8a.zip"
# )
# 
# for f in "${release_files[@]}"; do
#     wget "${release_url}/${release_ver}/${f}" -O "${f}"
# done

release_url="https://github.com/v2fly/v2ray-core/releases/download"
release_ver="v4.45.0"
declare -a release_files=(
    "v2ray-linux-64.zip"
    "v2ray-macos-64.zip"
    "v2ray-windows-64.zip"
    "v2ray-macos-arm64-v8a.zip"
    # "v2ray-linux-arm64-v8a.zip"
)

for f in "${release_files[@]}"; do
    wget "${release_url}/${release_ver}/${f}" -O "${f}"
done

wget "https://raw.githubusercontent.com/XTLS/Xray-install/main/install-release.sh" -O install-xray.sh
wget "https://raw.githubusercontent.com/v2fly/fhs-install-v2ray/master/install-release.sh" -O install-v2ray.sh

