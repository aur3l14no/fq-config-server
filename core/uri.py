import re
import json
from .utils import base64_decode, base64_encode


# class Outbound:
#     def __init__(self) -> None:
#         self.name = ''
#         self.uri: URI = None
#         self.data = {}


class URI:
    def __init__(self, uri: str) -> None:
        self.uri: str = uri

    @staticmethod
    def from_str(s: str):
        if s.startswith('vmess://'):
            return VmessURI(s)
        elif s.startswith('vless://'):
            return VlessURI(s)

    def get_name(self) -> str:
        raise NotImplementedError

    def update_name(self) -> None:
        raise NotImplementedError

    def __str__(self) -> str:
        return f'{type(self)} {self.uri}'

    def __repr__(self) -> str:
        return self.__str__()


class VmessURI(URI):
    def __init__(self, uri) -> None:
        super().__init__(uri)

    def get_name(self) -> str:
        code = self.uri.replace('vmess://', '')
        dict_str = base64_decode(code)
        dict = json.loads(dict_str)
        return dict['ps']

    def update_name(self, name) -> None:
        code = self.uri.replace('vmess://', '')
        dict_str = base64_decode(code)
        dict = json.loads(dict_str)
        dict['ps'] = name
        dict_str = json.dumps(dict)
        self.uri = 'vmess://' + base64_encode(dict_str)


class VlessURI(URI):
    def __init__(self, uri) -> None:
        super().__init__(uri)

    def get_name(self) -> str:
        try:
            return re.search(r'#(.+)$', self.uri).group(1)
        except:
            raise Exception(f'Illegal VLESS URI: {self.uri}')

    def update_name(self, name) -> None:
        try:
            self.uri = re.sub(r'#(.+)$', '#' + name, self.uri)
        except:
            raise Exception(f'Illegal VLESS URI: {self.uri}')
