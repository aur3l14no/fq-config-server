import base64
import operator
from functools import reduce
from types import FunctionType
from typing import Iterable


def base64_encode(s: str) -> str:
    return base64.b64encode(s.encode('utf8')).decode('utf8')


def base64_decode(s: str) -> str:
    return base64.b64decode(s).decode('utf8')


def filtermap(func: FunctionType, iterable: Iterable):
    return filter(lambda x: x != None, map(func, iterable))


def dict_from_items(iterable: Iterable, reduce_func: FunctionType = operator.add):
    return reduce(
        lambda acc, item:
        acc | {item[0]: reduce_func(acc[item[0]], item[1])
               if item[0] in acc
               else item[1]},
        iterable, {})
