import yaml
import requests
import re
from typing import List, Tuple
from functools import partial
from itertools import chain
from collections import OrderedDict
from flask import Flask, request, abort, send_from_directory
from core.uri import URI
from core.utils import base64_decode, base64_encode, dict_from_items, filtermap


proxy_group = {}


def process_single_proxy_config(proxy) -> Tuple[str, List[URI]]:
    """Single proxy config to (group_name, uris)

    Args:
        proxy: single proxy entry in `config.yaml`

    Returns:
        Tuple[str, List[URI]]: (group_name, uris)
    """

    def process_uri(uri: URI, regex, rename_from, rename_to):
        if regex == None or re.match(regex, uri.get_name()):
            if rename_from != None and rename_to != None:
                uri.update_name(re.sub(rename_from, rename_to, uri.get_name()))
            return uri

    name = proxy['name']
    uris = []
    if proxy['type'] == 'subscribe':
        try:
            r = requests.get(proxy['url'])
            uri_strs = base64_decode(r.text).splitlines()
            uris = list(filtermap(URI.from_str, uri_strs))
        except:
            pass
    if proxy['type'] in ('vmess', 'vless'):
        uris = [URI.from_str(proxy['url'])]
    uris = list(filtermap(partial(process_uri,
                                  regex=proxy.get('regex'),
                                  rename_from=proxy.get('rename_from'),
                                  rename_to=proxy.get('rename_to')), uris))
    return name, uris


def update_proxy_group():
    global proxy_group
    with open('config.yaml') as f:
        config = yaml.safe_load(f.read())
    processed_proxies = [process_single_proxy_config(proxy)
                         for proxy in config['proxies']]
    proxy_group = dict_from_items(processed_proxies)


########## Flask ##########


app = Flask(__name__)


def require_auth(f):
    with open('config.yaml') as fp:
        token = yaml.safe_load(fp.read())['token']

    def g(*args, **kwargs):
        x = request.args.get('token', '')
        if x != token:
            abort(401)
        return f(*args, **kwargs)
    g.__name__ = f.__name__
    return g


@ app.route('/')
def hello():
    return r'''Available: v2ray-linux-64.zip, v2ray-macos-64.zip, v2ray-windows-64.zip, v2ray-macos-arm64-v8a.zip

HOST=""
SOFTWARE="v2ray-linux-64.zip"
TOKEN=""

wget http://${HOST}/software/${SOFTWARE}?token=${TOKEN} -O ${SOFTWARE}
wget http://${HOST}/software/install-v2ray.sh?token=${TOKEN} -O install.sh

bash install.sh -l ${SOFTWARE}

wget http://${HOST}/config/config.json?token=${TOKEN} -O /usr/local/etc/v2ray/config.json

sudo systemctl enable v2ray.service
sudo systemctl restart v2ray.service
'''


@ app.route('/stat')
@ require_auth
def stat():
    with open('config.yaml') as f:
        return f.read()


@ app.route('/subscribe')
@ app.route('/sub')
@ require_auth
def route_subscription():
    x = request.args.get('names') or 'share'
    enables = set(str.lower(x).split(','))
    update_proxy_group()
    enabled_proxy_groups = {k: v
                            for k, v in proxy_group.items()
                            if k in enables}
    uri_strs = [u.uri for u in
                OrderedDict.fromkeys(
                    chain.from_iterable(enabled_proxy_groups.values()))]
    return base64_encode('\n'.join(uri_strs))


@ app.route('/config/<path:name>')
@ require_auth
def route_download_config(name):
    return send_from_directory('configs', name)


@ app.route('/software/<path:name>')
@ require_auth
def route_download_software(name):
    return send_from_directory('software', name)


if __name__ == '__main__':
    with open('config.yaml') as f:
        config = yaml.safe_load(f.read())
    port = config['port']
    # update_proxy_group()
    app.run(host='0.0.0.0', port=port, debug=True)
